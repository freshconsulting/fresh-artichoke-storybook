FROM node:carbon

WORKDIR /app
ADD . /app

RUN npm install
RUN npm run build-storybook

EXPOSE 6006

CMD ["npx", "serve", "-p", "6006", "/app/storybook-static"]
