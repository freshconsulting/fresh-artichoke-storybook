# Fresh Artichoke Storybook

A [React Storybook](https://storybook.js.org/) for Fresh Artichoke and other projects to consume to build apps.

## Setup

Run `docker-compose up`

## Develop

`https://github.com/storybooks/storybook/tree/master/app/react`

## Publish

Run `npm version` for small changes or use `npm version minor` and `npm version major` appropriately.
