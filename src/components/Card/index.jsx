import PropTypes from 'prop-types';
import React from 'react';
import {css, cx} from 'emotion';
import * as colors from '../../styles/colors';

/**
 * The Card Component creates a shadowed box with standard padding
 * that encapsulates other elements
 */

const Card = (
  {
    children,
    className,
    selected
  }
) => {
  const cardStyle = css`
    width: 100%;
    background-color: ${colors.bg};
    border-radius: 5px;
    display: inline-block;
    position: relative;
    box-shadow: 1px 1px 2px 0 ${colors.bgMedDark}, 2px 2px 15px 1px ${colors.bgMed};
    overflow: hidden;
  `;

  const selectedStyle = css`
      border: 2px solid ${colors.primary};
      background-color: ${colors.lightPrimary};
    `;

  const classes = cx(
    cardStyle,
    {[selectedStyle]: selected},
    className
  );
  return (
    <div className={classes}>
      {children}
    </div>
  );
};

Card.propTypes = {
  children: PropTypes.any.isRequired,
  className: PropTypes.string,
  selected: PropTypes.bool
}

Card.defaultProps = {
  selected: false
}

export default Card;
