import PropTypes from 'prop-types';
import React from 'react';
import {css, cx} from 'emotion';

const CardContent = (
  {
    children,
    className
  }
) => {
  const contentStyle = css`
     padding: 1rem;
  `;

  return (
    <div className={cx(contentStyle, className)}>
      {children}
    </div>
  );
};

CardContent.propTypes = {
  children: PropTypes.any.isRequired,
  className: PropTypes.string
}

export default CardContent;
