import PropTypes from 'prop-types';
import React from 'react';
import {css, cx} from 'emotion';
import * as colors from '../../../styles/colors';

/**
 * Header section that is visually separated from the rest of the Card
 */

const CardHeader = (
  {
    children,
    className
  }
) => {
  const headerStyle = css`
    padding: 1rem 1rem 0;
    border-bottom: 1px solid ${colors.border};
  `;

  return (
    <div className={cx(headerStyle, className)}>
      {children}
    </div>
  );
};

CardHeader.propTypes = {
  children: PropTypes.any.isRequired,
  className: PropTypes.string
}

export default CardHeader;
