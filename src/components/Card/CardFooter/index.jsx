import PropTypes from 'prop-types';
import React from 'react';
import {css, cx} from 'emotion';
import * as colors from '../../../styles/colors';

/**
 * Footer section that is visually separated from the rest of the Card
 */

const CardFooter = (
  {
    children,
    active,
    className
  }
) => {
  const footerStyle = css`
    padding: 0.5rem 1rem;
  `;

  const activeStyle = css`
    background: ${colors.lightPrimary};
  `;

  const inactiveStyle = css`
    border-top: 1px solid ${colors.border};
  `;

  return (
    <div className={cx(footerStyle, (active ? activeStyle : inactiveStyle), className)}>
      {children}
    </div>
  );
};

CardFooter.propTypes = {
  active: PropTypes.bool,
  children: PropTypes.any.isRequired,
  className: PropTypes.string
}

CardFooter.defaultProps = {
  active: false
}

export default CardFooter;
