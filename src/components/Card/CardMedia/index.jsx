import PropTypes from 'prop-types';
import React from 'react';
import {css, cx} from 'emotion';

/** 
 * Header section that is visually separated from the rest of the Card
*/

const CardMedia = (
  {
    children,
    className
  }
) => {
  const mediaStyle = css`
    width:100%;
    display: flex;
    img, .img {
      max-height:300px;
      width: 100%;
      object-fit: cover;
    }
  `;

  return (
    <div className={cx(mediaStyle, className)}>
      {children}
    </div>
  );
};

CardMedia.propTypes = {
  children: PropTypes.any.isRequired,
  className: PropTypes.string
}

export default CardMedia;
