/* Local Named Colors */
const jade = '#00c45d';
const hazyFieldGreen = '#94d6b3';
const mintCream = '#e7fff2';
const deepBlush = '#ff7282';
const cerulean = '#00afcc';
const lakeErieBlue = '#9cbcc2';
const constructionCone = '#f98b05';

const straightUpWhite = '#ffffff';
const straightUpBlack = '#000000';
const dustBunnyGray = '#989898';
const slateGray = '#748893';
const petroleum = '#748893';
const silverSand = '#b5bcc0';

const mercury = '#e1e1e1';
const concrete = '#e6e6e6';
const porcelain = '#ecedee';
const whisperGray = '#f4f6f6';
const alabaster = '#f9f9f9';

/* Semantic Colors */
// general thematic colors
export const primary = jade;
export const medPrimary = hazyFieldGreen;
export const lightPrimary = mintCream;
export const accent = constructionCone;
export const border = mercury;

// background colors
export const bg = straightUpWhite;
export const bgDark = petroleum;
export const bgMedDark = concrete;
export const bgMed = porcelain;
export const bgMedLight = whisperGray;
export const bgLight = alabaster;

// typographical colors
export const text = straightUpBlack;
export const textDark = dustBunnyGray;
export const textMed = slateGray;
export const textLight = silverSand;

// specific functional colors
export const notification = deepBlush;
export const link = cerulean;
export const iconButton = lakeErieBlue;
export const love = deepBlush;
