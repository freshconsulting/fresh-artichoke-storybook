import React from 'react';

import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';
import { withInfo } from '@storybook/addon-info';
import figmaDecorator from 'storybook-addon-figma';

storiesOf('Search', module)
  .addDecorator(figmaDecorator({
    url: 'https://www.figma.com/file/QH7KKEqjq0l6gShVtV1aZRsN/Fresh-Artichoke-DLS?node-id=12%3A52'
  }))
  .add(
    'Master Search',
    withInfo({
      text: `Master search field`
    }) (() =>
      <div></div>
    ))
