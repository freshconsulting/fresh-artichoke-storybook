import React from 'react';

import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';
import { withInfo } from '@storybook/addon-info';
import figmaDecorator from 'storybook-addon-figma';
import { withKnobs, boolean } from '@storybook/addon-knobs';

import Card from '../src/components/Card';
import CardContent from '../src/components/Card/CardContent';
import CardHeader from '../src/components/Card/CardHeader';
import CardFooter from '../src/components/Card/CardFooter';
import CardMedia from '../src/components/Card/CardMedia';

const imgSrc = `https://images.fastcompany.net/image/upload/w_937,ar_16:9,c_fill,g_auto,f_auto,q_auto,fl_lossy/wp-cms/uploads/2017/12/p-1-mushroom-elixir.jpg`;

storiesOf('Cards', module)
  .addDecorator(withKnobs)
  .addDecorator(figmaDecorator({
    url: 'https://www.figma.com/file/QH7KKEqjq0l6gShVtV1aZRsN/Fresh-Artichoke-DLS?node-id=19%3A3'
  }))
  .add(
    'Standard',
    withInfo({
      text: `The standard usage of a card`
    }) (() =>
      <Card selected={boolean('Card Selected', false)}>
        <CardHeader>
          <h1>Charles McIlvaine</h1>
        </CardHeader>
        <CardMedia>
          <img src={imgSrc} alt="beautiful mushrooms"/>
        </CardMedia>
        <CardContent>
          <p>
            McIlvaine compiled his notes into the book One Thousand American Fungi, still named as a
            "classic" work of American mycology.
            He is remembered for his writings supporting the edibility and dietary value of mushrooms.
            He presided over the Philadelphia Mycological Center which published a bulletin of his results.
            He consumed hundreds of species, including some (such as the acrid Russula emetica and bitter
            Hypholoma fasciculare) that are generally considered poisonous, earning him the nickname 'Ole
            Ironguts'.
            His experimentation was not without caution, however, and he did not die of mushroom poisoning,
            but of natural causes.
          </p>
        </CardContent>
        <CardFooter active={boolean('Footer Active', true)}>
          <p><em>he did not die of mushroom poisoning, but of natural causes</em></p>
        </CardFooter>
      </Card>
    ))
